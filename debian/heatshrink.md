% heatshrink(1) | User Commands
%
% "December  5 2023"

# NAME

heatshrink - compress/decompress byte streams using LZSS

# SYNOPSIS

**heatshrink** [**-h**]

**heatshrink** [**-e** | **-d**] [**-v**] [**-w** _SIZE_] [**-l** _BITS_] [_IN\_FILE_] [_OUT\_FILE_]

# DESCRIPTION

**heatshrink** compresses or decompresses byte streams using LZSS and is designed especially for embedded, low-memory, and/or hard real-time systems.

# OPTIONS

**-h**
:   Print help

**-e**
:   Encode (compress, default)

**-d**
:   Decode (decompress)

**-v**
:   Verbose (print input & output sizes, compression ratio, etc.)

**-w** _SIZE_
:   Base-2 log of LZSS sliding window size
    A larger value allows searches a larger history of the data for
    repeated patterns, potentially compressing more effectively, but will
    use more memory and processing time. Recommended default: **-w** _8_
    (embedded systems), **-w** _10_ (elsewhere)

**-l** _BITS_
:   Number of bits used for back-reference lengths. A larger value allows
    longer substitutions, but since all back-references must use **-w** +
    **-l** bits, larger **-w** or **-l** can be counterproductive if most
    patterns are small and/or local. Recommended default: **-l** _4_

If _IN\_FILE_ or _OUT\_FILE_ are unspecified, they will default to _-_ for standard input and standard output, respectively.

# BUGS

The upstream BTS can be found at https://github.com/atomicobject/heatshrink/issues

# AUTHOR

Chow Loong Jin <hyperair@debian.org>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2023 Chow Loong Jin

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

[comment]: #  Local Variables:
[comment]: #  mode: markdown
[comment]: #  End:
